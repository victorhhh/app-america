import 'dart:io';

import 'package:empresa/PdfPreviewScreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;

class Stepi extends StatefulWidget {
  Stepi({Key key}) : super(key: key);

  @override
  _StepiState createState() => _StepiState();
}

class _StepiState extends State<Stepi> {
  int _currentStep = 0;
  bool _complete = false;
  final pdf = pw.Document();

  final _nombre = TextEditingController();
  final _mision = TextEditingController();
  final _vision = TextEditingController();
  final _valores = TextEditingController();
  final _objetivoLargo1 = TextEditingController();
  final _objetivoLargo2 = TextEditingController();
  final _objetivoLargo3 = TextEditingController();
  final _objetivoMediano1 = TextEditingController();
  final _objetivoMediano2 = TextEditingController();
  final _objetivoMediano3 = TextEditingController();
  final _objetivoMediano4 = TextEditingController();
  final _objetivoMediano5 = TextEditingController();
  final _objetivoMediano6 = TextEditingController();
  final _objetivoMediano7 = TextEditingController();
  final _objetivoMediano8 = TextEditingController();
  final _objetivoMediano9 = TextEditingController();
  final _objetivoCorto1 = TextEditingController();
  final _objetivoCorto2 = TextEditingController();
  final _objetivoCorto3 = TextEditingController();
  final _objetivoCorto4 = TextEditingController();
  final _objetivoCorto5 = TextEditingController();
  final _objetivoCorto6 = TextEditingController();
  final _objetivoCorto7 = TextEditingController();
  final _objetivoCorto8 = TextEditingController();
  final _objetivoCorto9 = TextEditingController();
  final _objetivoCorto10 = TextEditingController();
  final _objetivoCorto11 = TextEditingController();
  final _objetivoCorto12 = TextEditingController();
  final _objetivoCorto13 = TextEditingController();
  final _objetivoCorto14 = TextEditingController();
  final _objetivoCorto15 = TextEditingController();
  final _objetivoCorto16 = TextEditingController();
  final _objetivoCorto17 = TextEditingController();
  final _objetivoCorto18 = TextEditingController();
  final _objetivoCorto19 = TextEditingController();
  final _objetivoCorto20 = TextEditingController();
  final _objetivoCorto21 = TextEditingController();
  final _objetivoCorto22 = TextEditingController();
  final _objetivoCorto23 = TextEditingController();
  final _objetivoCorto24 = TextEditingController();
  final _objetivoCorto25 = TextEditingController();
  final _objetivoCorto26 = TextEditingController();
  final _objetivoCorto27 = TextEditingController();

  final _f1 = TextEditingController();
  final _f2 = TextEditingController();
  final _f3 = TextEditingController();
  final _f4 = TextEditingController();
  final _f5 = TextEditingController();
  final _d1 = TextEditingController();
  final _d2 = TextEditingController();
  final _d3 = TextEditingController();
  final _d4 = TextEditingController();
  final _d5 = TextEditingController();
  final _o1 = TextEditingController();
  final _o2 = TextEditingController();
  final _o3 = TextEditingController();
  final _o4 = TextEditingController();
  final _o5 = TextEditingController();
  final _a1 = TextEditingController();
  final _a2 = TextEditingController();
  final _a3 = TextEditingController();
  final _a4 = TextEditingController();
  final _a5 = TextEditingController();

  final _p1 = TextEditingController();
  final _p2 = TextEditingController();
  final _p3 = TextEditingController();
  final _p4 = TextEditingController();
  final _p5 = TextEditingController();
  final _p6 = TextEditingController();
  final _p7 = TextEditingController();
  final _p8 = TextEditingController();
  final _p9 = TextEditingController();
  final _p10 = TextEditingController();
  final _p11 = TextEditingController();
  final _p12 = TextEditingController();
  final _p13 = TextEditingController();
  final _p14 = TextEditingController();
  final _p15 = TextEditingController();
  final _p16 = TextEditingController();
  final _p17 = TextEditingController();
  final _p18 = TextEditingController();
  final _p19 = TextEditingController();
  final _p20 = TextEditingController();

  String dropDownValue1 = '1';
  String dropDownValue2 = '1';
  String dropDownValue3 = '1';
  String dropDownValue4 = '1';
  String dropDownValue5 = '1';
  String dropDownValue6 = '1';
  String dropDownValue7 = '1';
  String dropDownValue8 = '1';
  String dropDownValue9 = '1';
  String dropDownValue10 = '1';
  String dropDownValue11 = '1';
  String dropDownValue12 = '1';
  String dropDownValue13 = '1';
  String dropDownValue14 = '1';
  String dropDownValue15 = '1';
  String dropDownValue16 = '1';
  String dropDownValue17 = '1';
  String dropDownValue18 = '1';
  String dropDownValue19 = '1';
  String dropDownValue20 = '1';

  sigmaF() {
    return ((double.parse(_p5.text) * double.parse(dropDownValue5)) +
        (double.parse(_p4.text) * double.parse(dropDownValue4)) +
        (double.parse(_p3.text) + double.parse(dropDownValue3)) +
        (double.parse(_p2.text) * double.parse(dropDownValue2)) +
        (double.parse(_p1.text) * double.parse(dropDownValue1)));
  }

  sigmaD() {
    return ((double.parse(_p6.text) * double.parse(dropDownValue6)) +
        (double.parse(_p7.text) * double.parse(dropDownValue7)) +
        (double.parse(_p8.text) + double.parse(dropDownValue8)) +
        (double.parse(_p9.text) * double.parse(dropDownValue9)) +
        (double.parse(_p10.text) * double.parse(dropDownValue10)));
  }

  sigmaO() {
    return ((double.parse(_p15.text) * double.parse(dropDownValue15)) +
        (double.parse(_p14.text) * double.parse(dropDownValue14)) +
        (double.parse(_p13.text) + double.parse(dropDownValue13)) +
        (double.parse(_p12.text) * double.parse(dropDownValue12)) +
        (double.parse(_p11.text) * double.parse(dropDownValue11)));
  }

  sigmaA() {
    return ((double.parse(_p20.text) * double.parse(dropDownValue20)) +
        (double.parse(_p19.text) * double.parse(dropDownValue19)) +
        (double.parse(_p18.text) + double.parse(dropDownValue18)) +
        (double.parse(_p17.text) * double.parse(dropDownValue17)) +
        (double.parse(_p16.text) * double.parse(dropDownValue16)));
  }

  coordenada1() {
    return sigmaF() - sigmaD();
  }

  coordenada2() {
    return sigmaO() - sigmaA();
  }

  determinarEstrategia() {
    if (coordenada1() > 0.0 && coordenada2() > 0.0) {
      return 'La diferencia de los pesos ponderados de fortalezas y debilidades es: ' +
          coordenada1().toString() +
          '. De igual forma la direncia entre las oportunidades y las amenazas corresponde a: ' +
          coordenada2().toString() +
          ' por lo tanto se ha determinado conveniente seguir una estrategia de manera AGRESIVA';
    }
    if (coordenada1() < 0.0 && coordenada2() > 0.0) {
      return 'La diferencia de los pesos ponderados de fortalezas y debilidades es: ' +
          coordenada1().toString() +
          '. De igual forma la direncia entre las oportunidades y las amenazas corresponde a: ' +
          coordenada2().toString() +
          ' por lo tanto se ha determinado conveniente seguir una estrategia de manera CONSERVADORA';
    }
    if (coordenada1() < 0.0 && coordenada2() < 0.0) {
      return 'La diferencia de los pesos ponderados de fortalezas y debilidades es: ' +
          coordenada1().toString() +
          '. De igual forma la direncia entre las oportunidades y las amenazas corresponde a: ' +
          coordenada2().toString() +
          ' por lo tanto se ha determinado conveniente seguir una estrategia de manera DEFENSIVA';
    }
    if (coordenada1() > 0.0 && coordenada2() < 0.0) {
      return 'La diferencia de los pesos ponderados de fortalezas y debilidades es: ' +
          coordenada1().toString() +
          '. De igual forma la direncia entre las oportunidades y las amenazas corresponde a: ' +
          coordenada2().toString() +
          ' por lo tanto se ha determinado conveniente seguir una estrategia de manera COMPETITIVA';
    }
  }

  writeOnPdf() {
    pdf.addPage(pw.MultiPage(
      pageFormat: PdfPageFormat.a4,
      margin: pw.EdgeInsets.all(32),
      build: (pw.Context context) {
        return <pw.Widget>[
          pw.Header(
            level: 0,
            text: "Identidad intitucional",
            /*textStyle: pw.TextStyle(fontSize: 30),*/
          ),
          pw.Header(
            level: 1,
            text: "Nombre de la empresa",
            /*style: pw.TextStyle(fontSize: 20.0)*/
          ),
          pw.Paragraph(
            text: _nombre.text, /*style: pw.TextStyle(fontSize: 15.0)*/
          ),
          pw.Header(
            level: 1,
            text: "Misión", /*style: pw.TextStyle(fontSize: 20.0)*/
          ),
          pw.Paragraph(
            text: _mision.text, /*style: pw.TextStyle(fontSize: 15.0)*/
          ),
          pw.Header(
            level: 1,
            text: "Visión", /*style: pw.TextStyle(fontSize: 20.0)*/
          ),
          pw.Paragraph(
            text: _vision.text, /*style: pw.TextStyle(fontSize: 15.0)*/
          ),
          pw.Header(
            level: 1,
            text: "Valores", /*style: pw.TextStyle(fontSize: 20.0)*/
          ),
          pw.Paragraph(
            text: _valores.text, /*style: pw.TextStyle(fontSize: 15.0)*/
          ),
          pw.Header(
            level: 0,
            text: "Objetivos estratégicos",
            /*textStyle: pw.TextStyle(fontSize: 30.0)*/
          ),
          pw.Header(
            level: 1,
            text: "Objetivo a largo plazo",
            /*textStyle: pw.TextStyle(fontSize: 30.0)*/
          ),
          pw.Paragraph(
            text: _objetivoLargo1.text,
          ),
          pw.Header(
            level: 2,
            text: "Objetivos a mediano plazo",
            /*textStyle: pw.TextStyle(fontSize: 30.0)*/
          ),
          pw.Paragraph(
            text:
                _objetivoMediano1.text, /*style: pw.TextStyle(fontSize: 15.0)*/
          ),
          pw.Header(
            level: 3,
            text: "Objetivos a corto plazo",
            /*textStyle: pw.TextStyle(fontSize: 30.0)*/
          ),
          pw.Paragraph(
            text: _objetivoCorto1.text, /*style: pw.TextStyle(fontSize: 15.0)*/
          ),
          pw.Paragraph(
            text: _objetivoCorto2.text, /*style: pw.TextStyle(fontSize: 15.0)*/
          ),
          pw.Paragraph(
            text: _objetivoCorto3.text, /*style: pw.TextStyle(fontSize: 15.0)*/
          ),
          pw.Header(
            level: 2,
            text: "Objetivo a mediano plazo",
            /*textStyle: pw.TextStyle(fontSize: 30.0)*/
          ),
          pw.Paragraph(
            text:
                _objetivoMediano2.text, /*style: pw.TextStyle(fontSize: 15.0)*/
          ),
          pw.Header(
            level: 3,
            text: "Objetivos a corto plazo",
            /*textStyle: pw.TextStyle(fontSize: 30.0)*/
          ),
          pw.Paragraph(
            text: _objetivoCorto4.text, /*style: pw.TextStyle(fontSize: 15.0)*/
          ),
          pw.Paragraph(
            text: _objetivoCorto5.text, /*style: pw.TextStyle(fontSize: 15.0)*/
          ),
          pw.Paragraph(
            text: _objetivoCorto6.text, /*style: pw.TextStyle(fontSize: 15.0)*/
          ),
          pw.Header(
            level: 2,
            text: "Objetivo a mediano plazo",
            /*textStyle: pw.TextStyle(fontSize: 30.0)*/
          ),
          pw.Paragraph(
            text:
                _objetivoMediano3.text, /*style: pw.TextStyle(fontSize: 15.0)*/
          ),
          pw.Header(
            level: 3,
            text: "Objetivos a corto plazo",
            /*textStyle: pw.TextStyle(fontSize: 30.0)*/
          ),
          pw.Paragraph(
            text: _objetivoCorto7.text, /*style: pw.TextStyle(fontSize: 15.0)*/
          ),
          pw.Paragraph(
            text: _objetivoCorto8.text, /*style: pw.TextStyle(fontSize: 15.0)*/
          ),
          pw.Paragraph(
            text: _objetivoCorto9.text, /*style: pw.TextStyle(fontSize: 15.0)*/
          ),
          //----------------------------------------------------------------------------------------------
          pw.Header(
            level: 1,
            text: "Objetivo a largo plazo",
            /*textStyle: pw.TextStyle(fontSize: 30.0)*/
          ),
          pw.Paragraph(
            text: _objetivoLargo2.text,
          ),
          pw.Header(
            level: 2,
            text: "Objetivos a mediano plazo",
            /*textStyle: pw.TextStyle(fontSize: 30.0)*/
          ),
          pw.Paragraph(
            text:
                _objetivoMediano4.text, /*style: pw.TextStyle(fontSize: 15.0)*/
          ),
          pw.Header(
            level: 3,
            text: "Objetivos a corto plazo",
            /*textStyle: pw.TextStyle(fontSize: 30.0)*/
          ),
          pw.Paragraph(
            text: _objetivoCorto10.text, /*style: pw.TextStyle(fontSize: 15.0)*/
          ),
          pw.Paragraph(
            text: _objetivoCorto11.text, /*style: pw.TextStyle(fontSize: 15.0)*/
          ),
          pw.Paragraph(
            text: _objetivoCorto12.text, /*style: pw.TextStyle(fontSize: 15.0)*/
          ),
          pw.Header(
            level: 2,
            text: "Objetivo a mediano plazo",
            /*textStyle: pw.TextStyle(fontSize: 30.0)*/
          ),
          pw.Paragraph(
            text:
                _objetivoMediano5.text, /*style: pw.TextStyle(fontSize: 15.0)*/
          ),
          pw.Header(
            level: 3,
            text: "Objetivos a corto plazo",
            /*textStyle: pw.TextStyle(fontSize: 30.0)*/
          ),
          pw.Paragraph(
            text: _objetivoCorto13.text, /*style: pw.TextStyle(fontSize: 15.0)*/
          ),
          pw.Paragraph(
            text: _objetivoCorto14.text, /*style: pw.TextStyle(fontSize: 15.0)*/
          ),
          pw.Paragraph(
            text: _objetivoCorto15.text, /*style: pw.TextStyle(fontSize: 15.0)*/
          ),
          pw.Header(
            level: 2,
            text: "Objetivo a mediano plazo",
            /*textStyle: pw.TextStyle(fontSize: 30.0)*/
          ),
          pw.Paragraph(
            text:
                _objetivoMediano6.text, /*style: pw.TextStyle(fontSize: 15.0)*/
          ),
          pw.Header(
            level: 3,
            text: "Objetivos a corto plazo",
            /*textStyle: pw.TextStyle(fontSize: 30.0)*/
          ),
          pw.Paragraph(
            text: _objetivoCorto16.text, /*style: pw.TextStyle(fontSize: 15.0)*/
          ),
          pw.Paragraph(
            text: _objetivoCorto17.text, /*style: pw.TextStyle(fontSize: 15.0)*/
          ),
          pw.Paragraph(
            text: _objetivoCorto18.text, /*style: pw.TextStyle(fontSize: 15.0)*/
          ),
          //----------------------------------------------------------------------------------------------
          pw.Header(
            level: 1,
            text: "Objetivo a largo plazo",
            /*textStyle: pw.TextStyle(fontSize: 30.0)*/
          ),
          pw.Paragraph(
            text: _objetivoLargo2.text,
          ),
          pw.Header(
            level: 2,
            text: "Objetivos a mediano plazo",
            /*textStyle: pw.TextStyle(fontSize: 30.0)*/
          ),
          pw.Paragraph(
            text:
                _objetivoMediano7.text, /*style: pw.TextStyle(fontSize: 15.0)*/
          ),
          pw.Header(
            level: 3,
            text: "Objetivos a corto plazo",
            /*textStyle: pw.TextStyle(fontSize: 30.0)*/
          ),
          pw.Paragraph(
            text: _objetivoCorto19.text, /*style: pw.TextStyle(fontSize: 15.0)*/
          ),
          pw.Paragraph(
            text: _objetivoCorto20.text, /*style: pw.TextStyle(fontSize: 15.0)*/
          ),
          pw.Paragraph(
            text: _objetivoCorto21.text, /*style: pw.TextStyle(fontSize: 15.0)*/
          ),
          pw.Header(
            level: 2,
            text: "Objetivo a mediano plazo",
            /*textStyle: pw.TextStyle(fontSize: 30.0)*/
          ),
          pw.Paragraph(
            text:
                _objetivoMediano8.text, /*style: pw.TextStyle(fontSize: 15.0)*/
          ),
          pw.Header(
            level: 3,
            text: "Objetivos a corto plazo",
            /*textStyle: pw.TextStyle(fontSize: 30.0)*/
          ),
          pw.Paragraph(
            text: _objetivoCorto22.text, /*style: pw.TextStyle(fontSize: 15.0)*/
          ),
          pw.Paragraph(
            text: _objetivoCorto23.text, /*style: pw.TextStyle(fontSize: 15.0)*/
          ),
          pw.Paragraph(
            text: _objetivoCorto24.text, /*style: pw.TextStyle(fontSize: 15.0)*/
          ),
          pw.Header(
            level: 2,
            text: "Objetivo a mediano plazo",
            /*textStyle: pw.TextStyle(fontSize: 30.0)*/
          ),
          pw.Paragraph(
            text:
                _objetivoMediano8.text, /*style: pw.TextStyle(fontSize: 15.0)*/
          ),
          pw.Header(
            level: 3,
            text: "Objetivos a corto plazo",
            /*textStyle: pw.TextStyle(fontSize: 30.0)*/
          ),
          pw.Paragraph(
            text: _objetivoCorto25.text, /*style: pw.TextStyle(fontSize: 15.0)*/
          ),
          pw.Paragraph(
            text: _objetivoCorto26.text, /*style: pw.TextStyle(fontSize: 15.0)*/
          ),
          pw.Paragraph(
            text: _objetivoCorto27.text, /*style: pw.TextStyle(fontSize: 15.0)*/
          ),
          //-------------------------FIN DE LOS OBJETIVOS ESTRATEGISCOS-----------------------
          pw.Header(
            level: 0,
            text: "Diagnóstico de la organización",
            /*textStyle: pw.TextStyle(fontSize: 30),*/
          ),
          pw.Header(
            level: 2,
            text: "Fortalezas",
            /*textStyle: pw.TextStyle(fontSize: 30),*/
          ),
          pw.Table.fromTextArray(context: context, data: <List<String>>[
            <String>[
              'Factor a anlizar',
              'Peso',
              'Calificación',
              'Peso ponderado'
            ],
            <String>[
              _f1.text,
              _p1.text,
              dropDownValue1,
              (double.parse(_p1.text) * double.parse(dropDownValue1)).toString()
            ],
            <String>[
              _f2.text,
              _p2.text,
              dropDownValue2,
              (double.parse(_p2.text) * double.parse(dropDownValue2)).toString()
            ],
            <String>[
              _f3.text,
              _p3.text,
              dropDownValue3,
              (double.parse(_p3.text) * double.parse(dropDownValue3)).toString()
            ],
            <String>[
              _f4.text,
              _p4.text,
              dropDownValue4,
              (double.parse(_p4.text) * double.parse(dropDownValue4)).toString()
            ],
            <String>[
              _f5.text,
              _p5.text,
              dropDownValue5,
              (double.parse(_p5.text) * double.parse(dropDownValue5)).toString()
            ],
            <String>['', '', 'Suma total', sigmaA().toString()]
          ]),
          pw.Header(
            level: 2,
            text: "Debilidades",
            /*textStyle: pw.TextStyle(fontSize: 30),*/
          ),
          pw.Table.fromTextArray(context: context, data: <List<String>>[
            <String>[
              'Factor a anlizar',
              'Peso',
              'Calificación',
              'Peso ponderado'
            ],
            <String>[
              _d1.text,
              _p6.text,
              dropDownValue6,
              (double.parse(_p6.text) * double.parse(dropDownValue6)).toString()
            ],
            <String>[
              _d2.text,
              _p7.text,
              dropDownValue7,
              (double.parse(_p7.text) * double.parse(dropDownValue7)).toString()
            ],
            <String>[
              _d3.text,
              _p8.text,
              dropDownValue8,
              (double.parse(_p8.text) * double.parse(dropDownValue8)).toString()
            ],
            <String>[
              _d4.text,
              _p9.text,
              dropDownValue4,
              (double.parse(_p9.text) * double.parse(dropDownValue9)).toString()
            ],
            <String>[
              _d5.text,
              _p10.text,
              dropDownValue10,
              (double.parse(_p10.text) * double.parse(dropDownValue10))
                  .toString()
            ],
            <String>['', '', 'Suma total', sigmaD().toString()]
          ]),
          pw.Header(
            level: 2,
            text: "Oportunidades",
            /*textStyle: pw.TextStyle(fontSize: 30),*/
          ),
          pw.Table.fromTextArray(context: context, data: <List<String>>[
            <String>[
              'Factor a anlizar',
              'Peso',
              'Calificación',
              'Peso ponderado'
            ],
            <String>[
              _o1.text,
              _p11.text,
              dropDownValue11,
              (double.parse(_p11.text) * double.parse(dropDownValue11))
                  .toString()
            ],
            <String>[
              _o2.text,
              _p12.text,
              dropDownValue12,
              (double.parse(_p12.text) * double.parse(dropDownValue12))
                  .toString()
            ],
            <String>[
              _o3.text,
              _p13.text,
              dropDownValue13,
              (double.parse(_p13.text) * double.parse(dropDownValue13))
                  .toString()
            ],
            <String>[
              _o4.text,
              _p14.text,
              dropDownValue14,
              (double.parse(_p14.text) * double.parse(dropDownValue14))
                  .toString()
            ],
            <String>[
              _o5.text,
              _p15.text,
              dropDownValue15,
              (double.parse(_p15.text) * double.parse(dropDownValue15))
                  .toString()
            ],
            <String>['', '', 'Suma total', sigmaO().toString()]
          ]),
          pw.Header(
            level: 2,
            text: "Amenazas",
            /*textStyle: pw.TextStyle(fontSize: 30),*/
          ),
          pw.Table.fromTextArray(context: context, data: <List<String>>[
            <String>[
              'Factor a anlizar',
              'Peso',
              'Calificación',
              'Peso ponderado'
            ],
            <String>[
              _a1.text,
              _p16.text,
              dropDownValue16,
              (double.parse(_p16.text) * double.parse(dropDownValue16))
                  .toString()
            ],
            <String>[
              _a2.text,
              _p17.text,
              dropDownValue17,
              (double.parse(_p17.text) * double.parse(dropDownValue17))
                  .toString()
            ],
            <String>[
              _a3.text,
              _p18.text,
              dropDownValue18,
              (double.parse(_p18.text) * double.parse(dropDownValue18))
                  .toString()
            ],
            <String>[
              _a4.text,
              _p19.text,
              dropDownValue19,
              (double.parse(_p19.text) * double.parse(dropDownValue19))
                  .toString()
            ],
            <String>[
              _a5.text,
              _p20.text,
              dropDownValue20,
              (double.parse(_p20.text) * double.parse(dropDownValue20))
                  .toString()
            ],
            <String>['', '', 'Suma total', sigmaA().toString()]
          ]),
          pw.Header(
            level: 2,
            text: "Ruta estrategica",
            /*textStyle: pw.TextStyle(fontSize: 30),*/
          ),
          pw.Paragraph(
            text: determinarEstrategia(),
          ),
        ];
      },
    ));
  }

  Future savePdf() async {
    Directory documentDirectory = await getApplicationDocumentsDirectory();
    String documentPath = documentDirectory.path;
    File file = File("$documentPath/example.pdf");
    file.writeAsBytesSync(pdf.save());
  }

  // String dropDownValue = '1';

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
          title: Text("Beauty Skin"),
        ),
        body: Column(children: <Widget>[
          _complete
              ? Expanded(
                  child: Center(
                  child: AlertDialog(
                    title: Text("Finalizado"),
                    content: Text("Chevere"),
                    actions: [
                      FlatButton(
                        child: Text("Cerrar"),
                        onPressed: () => {setState(() => _complete = false)},
                      )
                    ],
                  ),
                ))
              : Expanded(
                  child: Stepper(
                      type: StepperType.horizontal,
                      currentStep: _currentStep,
                      controlsBuilder: (BuildContext context,
                          {VoidCallback onStepContinue,
                          VoidCallback onStepCancel}) {
                        return Row(
                          children: <Widget>[
                            SizedBox(height: 70.0),
                            Container(
                              color: Colors.blue,
                              child: FlatButton(
                                  onPressed: onStepContinue,
                                  child: _currentStep < 2
                                      ? Text(
                                          'Continuar',
                                          style: TextStyle(color: Colors.white),
                                        )
                                      : Text(
                                          'PDF',
                                          style: TextStyle(color: Colors.white),
                                        )),
                            ),
                            FlatButton(
                              onPressed: onStepCancel,
                              child: const Text('Atras'),
                            ),
                          ],
                        );
                      },
                      onStepTapped: (int step) =>
                          setState(() => _currentStep = step),
                      onStepContinue: _currentStep < 2
                          ? () => setState(() => _currentStep += 1)
                          : () async {
                              writeOnPdf();
                              await savePdf();
                              Directory documentDirectory =
                                  await getApplicationDocumentsDirectory();
                              String documentPath = documentDirectory.path;
                              String fullPath = "$documentPath/example.pdf";
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          PdfPreviewScreen(path: fullPath)));
                              //setState(() => _complete = true);
                            },
                      onStepCancel: _currentStep > 0
                          ? () => setState(() => _currentStep -= 1)
                          : null,
                      steps: <Step>[
                        new Step(
                          isActive: _currentStep >= 0,
                          state: _currentStep >= 0
                              ? StepState.complete
                              : StepState.disabled,
                          title: Text("Identidad"),
                          content: Column(
                            children: <Widget>[
                              TextFormField(
                                controller: _nombre,
                                decoration: InputDecoration(
                                    labelText: 'Nombre de la empresa'),
                              ),
                              TextFormField(
                                controller: _mision,
                                decoration:
                                    InputDecoration(labelText: 'Misión'),
                              ),
                              TextFormField(
                                controller: _vision,
                                decoration: InputDecoration(labelText: 'Vsión'),
                              ),
                              TextFormField(
                                controller: _valores,
                                decoration:
                                    InputDecoration(labelText: 'Valores'),
                              ),
                            ],
                          ),
                        ),
                        new Step(
                          title: Text('Objetivos'),
                          content: Column(
                            children: <Widget>[
                              TextFormField(
                                controller: _objetivoLargo1,
                                decoration: InputDecoration(
                                    labelText: 'Objetivo a largo plazo'),
                              ),
                              ExpansionTile(
                                title: Text("Objetivo a mediano plazo"),
                                children: [
                                  TextFormField(
                                    controller: _objetivoMediano1,
                                    decoration: InputDecoration(
                                        labelText:
                                            'Primer objetivo a mediano plazo'),
                                  ),
                                  ExpansionTile(
                                    title: Text("Objetivos a corto plazo"),
                                    children: [
                                      TextFormField(
                                        controller: _objetivoCorto1,
                                        decoration: InputDecoration(
                                            labelText:
                                                'Primero objetivo a corto plazo'),
                                      ),
                                      TextFormField(
                                        controller: _objetivoCorto2,
                                        decoration: InputDecoration(
                                            labelText:
                                                'Segundo objetivo a corto plazo'),
                                      ),
                                      TextFormField(
                                        controller: _objetivoCorto3,
                                        decoration: InputDecoration(
                                            labelText:
                                                'Tercer objetivo a corto plazo'),
                                      ),
                                    ],
                                  ),
                                  TextFormField(
                                    controller: _objetivoMediano2,
                                    decoration: InputDecoration(
                                        labelText:
                                            'Primer objetivo a mediano plazo'),
                                  ),
                                  ExpansionTile(
                                    title: Text("Objetivos a corto plazo"),
                                    children: [
                                      TextFormField(
                                        controller: _objetivoCorto4,
                                        decoration: InputDecoration(
                                            labelText:
                                                'Segundo objetivo a corto plazo'),
                                      ),
                                      TextFormField(
                                        controller: _objetivoCorto5,
                                        decoration: InputDecoration(
                                            labelText:
                                                'Segundo objetivo a corto plazo'),
                                      ),
                                      TextFormField(
                                        controller: _objetivoCorto6,
                                        decoration: InputDecoration(
                                            labelText:
                                                'Tercer objetivo a corto plazo'),
                                      ),
                                    ],
                                  ),
                                  TextFormField(
                                    controller: _objetivoMediano3,
                                    decoration: InputDecoration(
                                        labelText:
                                            'Tercer objetivo a mediano plazo'),
                                  ),
                                  ExpansionTile(
                                    title: Text("Objetivos a corto plazo"),
                                    children: [
                                      TextFormField(
                                        controller: _objetivoCorto7,
                                        decoration: InputDecoration(
                                            labelText:
                                                'Primero objetivo a corto plazo'),
                                      ),
                                      TextFormField(
                                        controller: _objetivoCorto8,
                                        decoration: InputDecoration(
                                            labelText:
                                                'Segundo objetivo a corto plazo'),
                                      ),
                                      TextFormField(
                                        controller: _objetivoCorto9,
                                        decoration: InputDecoration(
                                            labelText:
                                                'Tercer objetivo a corto plazo'),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                              TextFormField(
                                controller: _objetivoLargo2,
                                decoration: InputDecoration(
                                    labelText: 'Objetivo a largo plazo'),
                              ),
                              ExpansionTile(
                                title: Text("Objetivo a mediano plazo"),
                                children: [
                                  TextFormField(
                                    controller: _objetivoMediano4,
                                    decoration: InputDecoration(
                                        labelText:
                                            'Primer objetivo a mediano plazo'),
                                  ),
                                  ExpansionTile(
                                    title: Text("Objetivos a corto plazo"),
                                    children: [
                                      TextFormField(
                                        controller: _objetivoCorto10,
                                        decoration: InputDecoration(
                                            labelText:
                                                'Primero objetivo a corto plazo'),
                                      ),
                                      TextFormField(
                                        controller: _objetivoCorto11,
                                        decoration: InputDecoration(
                                            labelText:
                                                'Segundo objetivo a corto plazo'),
                                      ),
                                      TextFormField(
                                        controller: _objetivoCorto12,
                                        decoration: InputDecoration(
                                            labelText:
                                                'Tercer objetivo a corto plazo'),
                                      ),
                                    ],
                                  ),
                                  TextFormField(
                                    controller: _objetivoMediano5,
                                    decoration: InputDecoration(
                                        labelText:
                                            'Primer objetivo a mediano plazo'),
                                  ),
                                  ExpansionTile(
                                    title: Text("Objetivos a corto plazo"),
                                    children: [
                                      TextFormField(
                                        controller: _objetivoCorto13,
                                        decoration: InputDecoration(
                                            labelText:
                                                'Segundo objetivo a corto plazo'),
                                      ),
                                      TextFormField(
                                        controller: _objetivoCorto14,
                                        decoration: InputDecoration(
                                            labelText:
                                                'Segundo objetivo a corto plazo'),
                                      ),
                                      TextFormField(
                                        controller: _objetivoCorto15,
                                        decoration: InputDecoration(
                                            labelText:
                                                'Tercer objetivo a corto plazo'),
                                      ),
                                    ],
                                  ),
                                  TextFormField(
                                    controller: _objetivoMediano6,
                                    decoration: InputDecoration(
                                        labelText:
                                            'Tercer objetivo a mediano plazo'),
                                  ),
                                  ExpansionTile(
                                    title: Text("Objetivos a corto plazo"),
                                    children: [
                                      TextFormField(
                                        controller: _objetivoCorto16,
                                        decoration: InputDecoration(
                                            labelText:
                                                'Primero objetivo a corto plazo'),
                                      ),
                                      TextFormField(
                                        controller: _objetivoCorto17,
                                        decoration: InputDecoration(
                                            labelText:
                                                'Segundo objetivo a corto plazo'),
                                      ),
                                      TextFormField(
                                        controller: _objetivoCorto18,
                                        decoration: InputDecoration(
                                            labelText:
                                                'Tercer objetivo a corto plazo'),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                              TextFormField(
                                controller: _objetivoLargo3,
                                decoration: InputDecoration(
                                    labelText: 'Objetivo a largo plazo'),
                              ),
                              ExpansionTile(
                                title: Text("Objetivo a mediano plazo"),
                                children: [
                                  TextFormField(
                                    controller: _objetivoMediano7,
                                    decoration: InputDecoration(
                                        labelText:
                                            'Primer objetivo a mediano plazo'),
                                  ),
                                  ExpansionTile(
                                    title: Text("Objetivos a corto plazo"),
                                    children: [
                                      TextFormField(
                                        controller: _objetivoCorto19,
                                        decoration: InputDecoration(
                                            labelText:
                                                'Primero objetivo a corto plazo'),
                                      ),
                                      TextFormField(
                                        controller: _objetivoCorto20,
                                        decoration: InputDecoration(
                                            labelText:
                                                'Segundo objetivo a corto plazo'),
                                      ),
                                      TextFormField(
                                        controller: _objetivoCorto21,
                                        decoration: InputDecoration(
                                            labelText:
                                                'Tercer objetivo a corto plazo'),
                                      ),
                                    ],
                                  ),
                                  TextFormField(
                                    controller: _objetivoMediano8,
                                    decoration: InputDecoration(
                                        labelText:
                                            'Primer objetivo a mediano plazo'),
                                  ),
                                  ExpansionTile(
                                    title: Text("Objetivos a corto plazo"),
                                    children: [
                                      TextFormField(
                                        controller: _objetivoCorto22,
                                        decoration: InputDecoration(
                                            labelText:
                                                'Segundo objetivo a corto plazo'),
                                      ),
                                      TextFormField(
                                        controller: _objetivoCorto23,
                                        decoration: InputDecoration(
                                            labelText:
                                                'Segundo objetivo a corto plazo'),
                                      ),
                                      TextFormField(
                                        controller: _objetivoCorto24,
                                        decoration: InputDecoration(
                                            labelText:
                                                'Tercer objetivo a corto plazo'),
                                      ),
                                    ],
                                  ),
                                  TextFormField(
                                    controller: _objetivoMediano9,
                                    decoration: InputDecoration(
                                        labelText:
                                            'Tercer objetivo a mediano plazo'),
                                  ),
                                  ExpansionTile(
                                    title: Text("Objetivos a corto plazo"),
                                    children: [
                                      TextFormField(
                                        controller: _objetivoCorto25,
                                        decoration: InputDecoration(
                                            labelText:
                                                'Primero objetivo a corto plazo'),
                                      ),
                                      TextFormField(
                                        controller: _objetivoCorto26,
                                        decoration: InputDecoration(
                                            labelText:
                                                'Segundo objetivo a corto plazo'),
                                      ),
                                      TextFormField(
                                        controller: _objetivoCorto27,
                                        decoration: InputDecoration(
                                            labelText:
                                                'Tercer objetivo a corto plazo'),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ],
                          ),
                          isActive: _currentStep >= 0,
                          state: _currentStep >= 1
                              ? StepState.complete
                              : StepState.disabled,
                        ),
                        new Step(
                          title: new Text('FODA'),
                          content: Column(
                            children: <Widget>[
                              ExpansionTile(
                                title: Text("Fortalezas"),
                                children: [
                                  new Container(
                                      height: 200.0,
                                      padding: EdgeInsets.all(15),
                                      margin: const EdgeInsets.only(
                                          top: 10.0,
                                          left: 10.0,
                                          right: 10.0,
                                          bottom: 10.0),
                                      decoration: BoxDecoration(
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.5),
                                            offset: Offset(0.0, 3.0),
                                            blurRadius: 10.0,
                                            spreadRadius: 3.0,
                                          )
                                        ],
                                        color: Colors.white,
                                        borderRadius: new BorderRadius.all(
                                            Radius.circular(5.0)),
                                      ),
                                      child: Column(
                                        children: [
                                          TextFormField(
                                            controller: _f1,
                                            decoration: InputDecoration(
                                                labelText: "Fortaleza"),
                                          ),
                                          Row(
                                            children: [
                                              Expanded(child: Text("PESO")),
                                              Expanded(
                                                child: TextFormField(
                                                  controller: _p1,
                                                  decoration: InputDecoration(
                                                      labelText: "[0.0 - 1]"),
                                                ),
                                              )
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              Expanded(
                                                  child: Text("CALIFICACIÓN")),
                                              Expanded(
                                                  child: DropdownButton<String>(
                                                      value: dropDownValue1,
                                                      items: <String>[
                                                        '1',
                                                        '2',
                                                        '3',
                                                        '4'
                                                      ].map<
                                                              DropdownMenuItem<
                                                                  String>>(
                                                          (String value) {
                                                        return DropdownMenuItem<
                                                            String>(
                                                          child: Text(value),
                                                          value: value,
                                                        );
                                                      }).toList(),
                                                      onChanged:
                                                          (String newValue) {
                                                        setState(() {
                                                          dropDownValue1 =
                                                              newValue;
                                                        });
                                                      }))
                                            ],
                                          ),
                                        ],
                                      )),
                                  new Container(
                                      height: 200.0,
                                      padding: EdgeInsets.all(15),
                                      margin: const EdgeInsets.only(
                                          top: 10.0,
                                          left: 10.0,
                                          right: 10.0,
                                          bottom: 10.0),
                                      decoration: BoxDecoration(
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.5),
                                            offset: Offset(0.0, 3.0),
                                            blurRadius: 10.0,
                                            spreadRadius: 3.0,
                                          )
                                        ],
                                        color: Colors.white,
                                        borderRadius: new BorderRadius.all(
                                            Radius.circular(5.0)),
                                      ),
                                      child: Column(
                                        children: [
                                          TextFormField(
                                            controller: _f2,
                                            decoration: InputDecoration(
                                                labelText: "Fortaleza"),
                                          ),
                                          Row(
                                            children: [
                                              Expanded(child: Text("PESO")),
                                              Expanded(
                                                child: TextFormField(
                                                  controller: _p2,
                                                  decoration: InputDecoration(
                                                      labelText: "[0.0 - 1]"),
                                                ),
                                              )
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              Expanded(
                                                  child: Text("CALIFICACIÓN")),
                                              Expanded(
                                                  child: DropdownButton<String>(
                                                      value: dropDownValue2,
                                                      items: <String>[
                                                        '1',
                                                        '2',
                                                        '3',
                                                        '4'
                                                      ].map<
                                                              DropdownMenuItem<
                                                                  String>>(
                                                          (String value) {
                                                        return DropdownMenuItem<
                                                            String>(
                                                          child: Text(value),
                                                          value: value,
                                                        );
                                                      }).toList(),
                                                      onChanged:
                                                          (String newValue) {
                                                        setState(() {
                                                          dropDownValue2 =
                                                              newValue;
                                                        });
                                                      }))
                                            ],
                                          ),
                                        ],
                                      )),
                                  new Container(
                                      height: 200.0,
                                      padding: EdgeInsets.all(15),
                                      margin: const EdgeInsets.only(
                                          top: 10.0,
                                          left: 10.0,
                                          right: 10.0,
                                          bottom: 10.0),
                                      decoration: BoxDecoration(
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.5),
                                            offset: Offset(0.0, 3.0),
                                            blurRadius: 10.0,
                                            spreadRadius: 3.0,
                                          )
                                        ],
                                        color: Colors.white,
                                        borderRadius: new BorderRadius.all(
                                            Radius.circular(5.0)),
                                      ),
                                      child: Column(
                                        children: [
                                          TextFormField(
                                            controller: _f3,
                                            decoration: InputDecoration(
                                                labelText: "Fortaleza"),
                                          ),
                                          Row(
                                            children: [
                                              Expanded(child: Text("PESO")),
                                              Expanded(
                                                child: TextFormField(
                                                  controller: _p3,
                                                  decoration: InputDecoration(
                                                      labelText: "[0.0 - 1]"),
                                                ),
                                              )
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              Expanded(
                                                  child: Text("CALIFICACIÓN")),
                                              Expanded(
                                                  child: DropdownButton<String>(
                                                      value: dropDownValue3,
                                                      items: <String>[
                                                        '1',
                                                        '2',
                                                        '3',
                                                        '4'
                                                      ].map<
                                                              DropdownMenuItem<
                                                                  String>>(
                                                          (String value) {
                                                        return DropdownMenuItem<
                                                            String>(
                                                          child: Text(value),
                                                          value: value,
                                                        );
                                                      }).toList(),
                                                      onChanged:
                                                          (String newValue) {
                                                        setState(() {
                                                          dropDownValue3 =
                                                              newValue;
                                                        });
                                                      }))
                                            ],
                                          ),
                                        ],
                                      )),
                                  new Container(
                                      height: 200.0,
                                      padding: EdgeInsets.all(15),
                                      margin: const EdgeInsets.only(
                                          top: 10.0,
                                          left: 10.0,
                                          right: 10.0,
                                          bottom: 10.0),
                                      decoration: BoxDecoration(
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.5),
                                            offset: Offset(0.0, 3.0),
                                            blurRadius: 10.0,
                                            spreadRadius: 3.0,
                                          )
                                        ],
                                        color: Colors.white,
                                        borderRadius: new BorderRadius.all(
                                            Radius.circular(5.0)),
                                      ),
                                      child: Column(
                                        children: [
                                          TextFormField(
                                            controller: _f4,
                                            decoration: InputDecoration(
                                                labelText: "Fortaleza"),
                                          ),
                                          Row(
                                            children: [
                                              Expanded(child: Text("PESO")),
                                              Expanded(
                                                child: TextFormField(
                                                  controller: _p4,
                                                  decoration: InputDecoration(
                                                      labelText: "[0.0 - 1]"),
                                                ),
                                              )
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              Expanded(
                                                  child: Text("CALIFICACIÓN")),
                                              Expanded(
                                                  child: DropdownButton<String>(
                                                      value: dropDownValue4,
                                                      items: <String>[
                                                        '1',
                                                        '2',
                                                        '3',
                                                        '4'
                                                      ].map<
                                                              DropdownMenuItem<
                                                                  String>>(
                                                          (String value) {
                                                        return DropdownMenuItem<
                                                            String>(
                                                          child: Text(value),
                                                          value: value,
                                                        );
                                                      }).toList(),
                                                      onChanged:
                                                          (String newValue) {
                                                        setState(() {
                                                          dropDownValue4 =
                                                              newValue;
                                                        });
                                                      }))
                                            ],
                                          ),
                                        ],
                                      )),
                                  new Container(
                                      height: 200.0,
                                      padding: EdgeInsets.all(15),
                                      margin: const EdgeInsets.only(
                                          top: 10.0,
                                          left: 10.0,
                                          right: 10.0,
                                          bottom: 10.0),
                                      decoration: BoxDecoration(
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.5),
                                            offset: Offset(0.0, 3.0),
                                            blurRadius: 10.0,
                                            spreadRadius: 3.0,
                                          )
                                        ],
                                        color: Colors.white,
                                        borderRadius: new BorderRadius.all(
                                            Radius.circular(5.0)),
                                      ),
                                      child: Column(
                                        children: [
                                          TextFormField(
                                            controller: _f5,
                                            decoration: InputDecoration(
                                                labelText: "Fortaleza"),
                                          ),
                                          Row(
                                            children: [
                                              Expanded(child: Text("PESO")),
                                              Expanded(
                                                child: TextFormField(
                                                  controller: _p5,
                                                  decoration: InputDecoration(
                                                      labelText: "[0.0 - 1]"),
                                                ),
                                              )
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              Expanded(
                                                  child: Text("CALIFICACIÓN")),
                                              Expanded(
                                                  child: DropdownButton<String>(
                                                      value: dropDownValue5,
                                                      items: <String>[
                                                        '1',
                                                        '2',
                                                        '3',
                                                        '4'
                                                      ].map<
                                                              DropdownMenuItem<
                                                                  String>>(
                                                          (String value) {
                                                        return DropdownMenuItem<
                                                            String>(
                                                          child: Text(value),
                                                          value: value,
                                                        );
                                                      }).toList(),
                                                      onChanged:
                                                          (String newValue) {
                                                        setState(() {
                                                          dropDownValue5 =
                                                              newValue;
                                                        });
                                                      }))
                                            ],
                                          ),
                                        ],
                                      )),
                                ],
                              ),
                              ExpansionTile(
                                title: Text("Debilidades"),
                                children: [
                                  new Container(
                                      height: 200.0,
                                      padding: EdgeInsets.all(15),
                                      margin: const EdgeInsets.only(
                                          top: 10.0,
                                          left: 10.0,
                                          right: 10.0,
                                          bottom: 10.0),
                                      decoration: BoxDecoration(
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.5),
                                            offset: Offset(0.0, 3.0),
                                            blurRadius: 10.0,
                                            spreadRadius: 3.0,
                                          )
                                        ],
                                        color: Colors.white,
                                        borderRadius: new BorderRadius.all(
                                            Radius.circular(5.0)),
                                      ),
                                      child: Column(
                                        children: [
                                          TextFormField(
                                            controller: _d1,
                                            decoration: InputDecoration(
                                                labelText: "Debilidad"),
                                          ),
                                          Row(
                                            children: [
                                              Expanded(child: Text("PESO")),
                                              Expanded(
                                                child: TextFormField(
                                                  controller: _p6,
                                                  decoration: InputDecoration(
                                                      labelText: "[0.0 - 1]"),
                                                ),
                                              )
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              Expanded(
                                                  child: Text("CALIFICACIÓN")),
                                              Expanded(
                                                  child: DropdownButton<String>(
                                                      value: dropDownValue6,
                                                      items: <String>[
                                                        '1',
                                                        '2',
                                                        '3',
                                                        '4'
                                                      ].map<
                                                              DropdownMenuItem<
                                                                  String>>(
                                                          (String value) {
                                                        return DropdownMenuItem<
                                                            String>(
                                                          child: Text(value),
                                                          value: value,
                                                        );
                                                      }).toList(),
                                                      onChanged:
                                                          (String newValue) {
                                                        setState(() {
                                                          dropDownValue6 =
                                                              newValue;
                                                        });
                                                      }))
                                            ],
                                          ),
                                        ],
                                      )),
                                  new Container(
                                      height: 200.0,
                                      padding: EdgeInsets.all(15),
                                      margin: const EdgeInsets.only(
                                          top: 10.0,
                                          left: 10.0,
                                          right: 10.0,
                                          bottom: 10.0),
                                      decoration: BoxDecoration(
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.5),
                                            offset: Offset(0.0, 3.0),
                                            blurRadius: 10.0,
                                            spreadRadius: 3.0,
                                          )
                                        ],
                                        color: Colors.white,
                                        borderRadius: new BorderRadius.all(
                                            Radius.circular(5.0)),
                                      ),
                                      child: Column(
                                        children: [
                                          TextFormField(
                                            controller: _d2,
                                            decoration: InputDecoration(
                                                labelText: "Debilidad"),
                                          ),
                                          Row(
                                            children: [
                                              Expanded(child: Text("PESO")),
                                              Expanded(
                                                child: TextFormField(
                                                  controller: _p7,
                                                  decoration: InputDecoration(
                                                      labelText: "[0.0 - 1]"),
                                                ),
                                              )
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              Expanded(
                                                  child: Text("CALIFICACIÓN")),
                                              Expanded(
                                                  child: DropdownButton<String>(
                                                      value: dropDownValue7,
                                                      items: <String>[
                                                        '1',
                                                        '2',
                                                        '3',
                                                        '4'
                                                      ].map<
                                                              DropdownMenuItem<
                                                                  String>>(
                                                          (String value) {
                                                        return DropdownMenuItem<
                                                            String>(
                                                          child: Text(value),
                                                          value: value,
                                                        );
                                                      }).toList(),
                                                      onChanged:
                                                          (String newValue) {
                                                        setState(() {
                                                          dropDownValue7 =
                                                              newValue;
                                                        });
                                                      }))
                                            ],
                                          ),
                                        ],
                                      )),
                                  new Container(
                                      height: 200.0,
                                      padding: EdgeInsets.all(15),
                                      margin: const EdgeInsets.only(
                                          top: 10.0,
                                          left: 10.0,
                                          right: 10.0,
                                          bottom: 10.0),
                                      decoration: BoxDecoration(
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.5),
                                            offset: Offset(0.0, 3.0),
                                            blurRadius: 10.0,
                                            spreadRadius: 3.0,
                                          )
                                        ],
                                        color: Colors.white,
                                        borderRadius: new BorderRadius.all(
                                            Radius.circular(5.0)),
                                      ),
                                      child: Column(
                                        children: [
                                          TextFormField(
                                            controller: _d3,
                                            decoration: InputDecoration(
                                                labelText: "Debilidad"),
                                          ),
                                          Row(
                                            children: [
                                              Expanded(child: Text("PESO")),
                                              Expanded(
                                                child: TextFormField(
                                                  controller: _p8,
                                                  decoration: InputDecoration(
                                                      labelText: "[0.0 - 1]"),
                                                ),
                                              )
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              Expanded(
                                                  child: Text("CALIFICACIÓN")),
                                              Expanded(
                                                  child: DropdownButton<String>(
                                                      value: dropDownValue8,
                                                      items: <String>[
                                                        '1',
                                                        '2',
                                                        '3',
                                                        '4'
                                                      ].map<
                                                              DropdownMenuItem<
                                                                  String>>(
                                                          (String value) {
                                                        return DropdownMenuItem<
                                                            String>(
                                                          child: Text(value),
                                                          value: value,
                                                        );
                                                      }).toList(),
                                                      onChanged:
                                                          (String newValue) {
                                                        setState(() {
                                                          dropDownValue8 =
                                                              newValue;
                                                        });
                                                      }))
                                            ],
                                          ),
                                        ],
                                      )),
                                  new Container(
                                      height: 200.0,
                                      padding: EdgeInsets.all(15),
                                      margin: const EdgeInsets.only(
                                          top: 10.0,
                                          left: 10.0,
                                          right: 10.0,
                                          bottom: 10.0),
                                      decoration: BoxDecoration(
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.5),
                                            offset: Offset(0.0, 3.0),
                                            blurRadius: 10.0,
                                            spreadRadius: 3.0,
                                          )
                                        ],
                                        color: Colors.white,
                                        borderRadius: new BorderRadius.all(
                                            Radius.circular(5.0)),
                                      ),
                                      child: Column(
                                        children: [
                                          TextFormField(
                                            controller: _d4,
                                            decoration: InputDecoration(
                                                labelText: "Debilidad"),
                                          ),
                                          Row(
                                            children: [
                                              Expanded(child: Text("PESO")),
                                              Expanded(
                                                child: TextFormField(
                                                  controller: _p9,
                                                  decoration: InputDecoration(
                                                      labelText: "[0.0 - 1]"),
                                                ),
                                              )
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              Expanded(
                                                  child: Text("CALIFICACIÓN")),
                                              Expanded(
                                                  child: DropdownButton<String>(
                                                      value: dropDownValue9,
                                                      items: <String>[
                                                        '1',
                                                        '2',
                                                        '3',
                                                        '4'
                                                      ].map<
                                                              DropdownMenuItem<
                                                                  String>>(
                                                          (String value) {
                                                        return DropdownMenuItem<
                                                            String>(
                                                          child: Text(value),
                                                          value: value,
                                                        );
                                                      }).toList(),
                                                      onChanged:
                                                          (String newValue) {
                                                        setState(() {
                                                          dropDownValue9 =
                                                              newValue;
                                                        });
                                                      }))
                                            ],
                                          ),
                                        ],
                                      )),
                                  new Container(
                                      height: 200.0,
                                      padding: EdgeInsets.all(15),
                                      margin: const EdgeInsets.only(
                                          top: 10.0,
                                          left: 10.0,
                                          right: 10.0,
                                          bottom: 10.0),
                                      decoration: BoxDecoration(
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.5),
                                            offset: Offset(0.0, 3.0),
                                            blurRadius: 10.0,
                                            spreadRadius: 3.0,
                                          )
                                        ],
                                        color: Colors.white,
                                        borderRadius: new BorderRadius.all(
                                            Radius.circular(5.0)),
                                      ),
                                      child: Column(
                                        children: [
                                          TextFormField(
                                            controller: _d5,
                                            decoration: InputDecoration(
                                                labelText: "Debilidad"),
                                          ),
                                          Row(
                                            children: [
                                              Expanded(child: Text("PESO")),
                                              Expanded(
                                                child: TextFormField(
                                                  controller: _p10,
                                                  decoration: InputDecoration(
                                                      labelText: "[0.0 - 1]"),
                                                ),
                                              )
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              Expanded(
                                                  child: Text("CALIFICACIÓN")),
                                              Expanded(
                                                  child: DropdownButton<String>(
                                                      value: dropDownValue10,
                                                      items: <String>[
                                                        '1',
                                                        '2',
                                                        '3',
                                                        '4'
                                                      ].map<
                                                              DropdownMenuItem<
                                                                  String>>(
                                                          (String value) {
                                                        return DropdownMenuItem<
                                                            String>(
                                                          child: Text(value),
                                                          value: value,
                                                        );
                                                      }).toList(),
                                                      onChanged:
                                                          (String newValue) {
                                                        setState(() {
                                                          dropDownValue10 =
                                                              newValue;
                                                        });
                                                      }))
                                            ],
                                          ),
                                        ],
                                      )),
                                ],
                              ),
                              ExpansionTile(
                                title: Text("Oportunidades"),
                                children: [
                                  new Container(
                                      height: 200.0,
                                      padding: EdgeInsets.all(15),
                                      margin: const EdgeInsets.only(
                                          top: 10.0,
                                          left: 10.0,
                                          right: 10.0,
                                          bottom: 10.0),
                                      decoration: BoxDecoration(
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.5),
                                            offset: Offset(0.0, 3.0),
                                            blurRadius: 10.0,
                                            spreadRadius: 3.0,
                                          )
                                        ],
                                        color: Colors.white,
                                        borderRadius: new BorderRadius.all(
                                            Radius.circular(5.0)),
                                      ),
                                      child: Column(
                                        children: [
                                          TextFormField(
                                            controller: _o1,
                                            decoration: InputDecoration(
                                                labelText: "Oportunidad"),
                                          ),
                                          Row(
                                            children: [
                                              Expanded(child: Text("PESO")),
                                              Expanded(
                                                child: TextFormField(
                                                  controller: _p11,
                                                  decoration: InputDecoration(
                                                      labelText: "[0.0 - 1]"),
                                                ),
                                              )
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              Expanded(
                                                  child: Text("CALIFICACIÓN")),
                                              Expanded(
                                                  child: DropdownButton<String>(
                                                      value: dropDownValue11,
                                                      items: <String>[
                                                        '1',
                                                        '2',
                                                        '3',
                                                        '4'
                                                      ].map<
                                                              DropdownMenuItem<
                                                                  String>>(
                                                          (String value) {
                                                        return DropdownMenuItem<
                                                            String>(
                                                          child: Text(value),
                                                          value: value,
                                                        );
                                                      }).toList(),
                                                      onChanged:
                                                          (String newValue) {
                                                        setState(() {
                                                          dropDownValue11 =
                                                              newValue;
                                                        });
                                                      }))
                                            ],
                                          ),
                                        ],
                                      )),
                                  new Container(
                                      height: 200.0,
                                      padding: EdgeInsets.all(15),
                                      margin: const EdgeInsets.only(
                                          top: 10.0,
                                          left: 10.0,
                                          right: 10.0,
                                          bottom: 10.0),
                                      decoration: BoxDecoration(
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.5),
                                            offset: Offset(0.0, 3.0),
                                            blurRadius: 10.0,
                                            spreadRadius: 3.0,
                                          )
                                        ],
                                        color: Colors.white,
                                        borderRadius: new BorderRadius.all(
                                            Radius.circular(5.0)),
                                      ),
                                      child: Column(
                                        children: [
                                          TextFormField(
                                            controller: _o2,
                                            decoration: InputDecoration(
                                                labelText: "Oportunidad"),
                                          ),
                                          Row(
                                            children: [
                                              Expanded(child: Text("PESO")),
                                              Expanded(
                                                child: TextFormField(
                                                  controller: _p12,
                                                  decoration: InputDecoration(
                                                      labelText: "[0.0 - 1]"),
                                                ),
                                              )
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              Expanded(
                                                  child: Text("CALIFICACIÓN")),
                                              Expanded(
                                                  child: DropdownButton<String>(
                                                      value: dropDownValue12,
                                                      items: <String>[
                                                        '1',
                                                        '2',
                                                        '3',
                                                        '4'
                                                      ].map<
                                                              DropdownMenuItem<
                                                                  String>>(
                                                          (String value) {
                                                        return DropdownMenuItem<
                                                            String>(
                                                          child: Text(value),
                                                          value: value,
                                                        );
                                                      }).toList(),
                                                      onChanged:
                                                          (String newValue) {
                                                        setState(() {
                                                          dropDownValue12 =
                                                              newValue;
                                                        });
                                                      }))
                                            ],
                                          ),
                                        ],
                                      )),
                                  new Container(
                                      height: 200.0,
                                      padding: EdgeInsets.all(15),
                                      margin: const EdgeInsets.only(
                                          top: 10.0,
                                          left: 10.0,
                                          right: 10.0,
                                          bottom: 10.0),
                                      decoration: BoxDecoration(
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.5),
                                            offset: Offset(0.0, 3.0),
                                            blurRadius: 10.0,
                                            spreadRadius: 3.0,
                                          )
                                        ],
                                        color: Colors.white,
                                        borderRadius: new BorderRadius.all(
                                            Radius.circular(5.0)),
                                      ),
                                      child: Column(
                                        children: [
                                          TextFormField(
                                            controller: _o3,
                                            decoration: InputDecoration(
                                                labelText: "Oportunidad"),
                                          ),
                                          Row(
                                            children: [
                                              Expanded(child: Text("PESO")),
                                              Expanded(
                                                child: TextFormField(
                                                  controller: _p13,
                                                  decoration: InputDecoration(
                                                      labelText: "[0.0 - 1]"),
                                                ),
                                              )
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              Expanded(
                                                  child: Text("CALIFICACIÓN")),
                                              Expanded(
                                                  child: DropdownButton<String>(
                                                      value: dropDownValue13,
                                                      items: <String>[
                                                        '1',
                                                        '2',
                                                        '3',
                                                        '4'
                                                      ].map<
                                                              DropdownMenuItem<
                                                                  String>>(
                                                          (String value) {
                                                        return DropdownMenuItem<
                                                            String>(
                                                          child: Text(value),
                                                          value: value,
                                                        );
                                                      }).toList(),
                                                      onChanged:
                                                          (String newValue) {
                                                        setState(() {
                                                          dropDownValue13 =
                                                              newValue;
                                                        });
                                                      }))
                                            ],
                                          ),
                                        ],
                                      )),
                                  new Container(
                                      height: 200.0,
                                      padding: EdgeInsets.all(15),
                                      margin: const EdgeInsets.only(
                                          top: 10.0,
                                          left: 10.0,
                                          right: 10.0,
                                          bottom: 10.0),
                                      decoration: BoxDecoration(
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.5),
                                            offset: Offset(0.0, 3.0),
                                            blurRadius: 10.0,
                                            spreadRadius: 3.0,
                                          )
                                        ],
                                        color: Colors.white,
                                        borderRadius: new BorderRadius.all(
                                            Radius.circular(5.0)),
                                      ),
                                      child: Column(
                                        children: [
                                          TextFormField(
                                            controller: _o4,
                                            decoration: InputDecoration(
                                                labelText: "Oportunidad"),
                                          ),
                                          Row(
                                            children: [
                                              Expanded(child: Text("PESO")),
                                              Expanded(
                                                child: TextFormField(
                                                  controller: _p14,
                                                  decoration: InputDecoration(
                                                      labelText: "[0.0 - 1]"),
                                                ),
                                              )
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              Expanded(
                                                  child: Text("CALIFICACIÓN")),
                                              Expanded(
                                                  child: DropdownButton<String>(
                                                      value: dropDownValue14,
                                                      items: <String>[
                                                        '1',
                                                        '2',
                                                        '3',
                                                        '4'
                                                      ].map<
                                                              DropdownMenuItem<
                                                                  String>>(
                                                          (String value) {
                                                        return DropdownMenuItem<
                                                            String>(
                                                          child: Text(value),
                                                          value: value,
                                                        );
                                                      }).toList(),
                                                      onChanged:
                                                          (String newValue) {
                                                        setState(() {
                                                          dropDownValue14 =
                                                              newValue;
                                                        });
                                                      }))
                                            ],
                                          ),
                                        ],
                                      )),
                                  new Container(
                                      height: 200.0,
                                      padding: EdgeInsets.all(15),
                                      margin: const EdgeInsets.only(
                                          top: 10.0,
                                          left: 10.0,
                                          right: 10.0,
                                          bottom: 10.0),
                                      decoration: BoxDecoration(
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.5),
                                            offset: Offset(0.0, 3.0),
                                            blurRadius: 10.0,
                                            spreadRadius: 3.0,
                                          )
                                        ],
                                        color: Colors.white,
                                        borderRadius: new BorderRadius.all(
                                            Radius.circular(5.0)),
                                      ),
                                      child: Column(
                                        children: [
                                          TextFormField(
                                            controller: _o5,
                                            decoration: InputDecoration(
                                                labelText: "Oportunidad"),
                                          ),
                                          Row(
                                            children: [
                                              Expanded(child: Text("PESO")),
                                              Expanded(
                                                child: TextFormField(
                                                  controller: _p15,
                                                  decoration: InputDecoration(
                                                      labelText: "[0.0 - 1]"),
                                                ),
                                              )
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              Expanded(
                                                  child: Text("CALIFICACIÓN")),
                                              Expanded(
                                                  child: DropdownButton<String>(
                                                      value: dropDownValue15,
                                                      items: <String>[
                                                        '1',
                                                        '2',
                                                        '3',
                                                        '4'
                                                      ].map<
                                                              DropdownMenuItem<
                                                                  String>>(
                                                          (String value) {
                                                        return DropdownMenuItem<
                                                            String>(
                                                          child: Text(value),
                                                          value: value,
                                                        );
                                                      }).toList(),
                                                      onChanged:
                                                          (String newValue) {
                                                        setState(() {
                                                          dropDownValue15 =
                                                              newValue;
                                                        });
                                                      }))
                                            ],
                                          ),
                                        ],
                                      )),
                                ],
                              ),
                              ExpansionTile(
                                title: Text("Amenazas"),
                                children: [
                                  new Container(
                                      height: 200.0,
                                      padding: EdgeInsets.all(15),
                                      margin: const EdgeInsets.only(
                                          top: 10.0,
                                          left: 10.0,
                                          right: 10.0,
                                          bottom: 10.0),
                                      decoration: BoxDecoration(
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.5),
                                            offset: Offset(0.0, 3.0),
                                            blurRadius: 10.0,
                                            spreadRadius: 3.0,
                                          )
                                        ],
                                        color: Colors.white,
                                        borderRadius: new BorderRadius.all(
                                            Radius.circular(5.0)),
                                      ),
                                      child: Column(
                                        children: [
                                          TextFormField(
                                            controller: _a1,
                                            decoration: InputDecoration(
                                                labelText: "Amenaza"),
                                          ),
                                          Row(
                                            children: [
                                              Expanded(child: Text("PESO")),
                                              Expanded(
                                                child: TextFormField(
                                                  controller: _p16,
                                                  decoration: InputDecoration(
                                                      labelText: "[0.0 - 1]"),
                                                ),
                                              )
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              Expanded(
                                                  child: Text("CALIFICACIÓN")),
                                              Expanded(
                                                  child: DropdownButton<String>(
                                                      value: dropDownValue16,
                                                      items: <String>[
                                                        '1',
                                                        '2',
                                                        '3',
                                                        '4'
                                                      ].map<
                                                              DropdownMenuItem<
                                                                  String>>(
                                                          (String value) {
                                                        return DropdownMenuItem<
                                                            String>(
                                                          child: Text(value),
                                                          value: value,
                                                        );
                                                      }).toList(),
                                                      onChanged:
                                                          (String newValue) {
                                                        setState(() {
                                                          dropDownValue16 =
                                                              newValue;
                                                        });
                                                      }))
                                            ],
                                          ),
                                        ],
                                      )),
                                  new Container(
                                      height: 200.0,
                                      padding: EdgeInsets.all(15),
                                      margin: const EdgeInsets.only(
                                          top: 10.0,
                                          left: 10.0,
                                          right: 10.0,
                                          bottom: 10.0),
                                      decoration: BoxDecoration(
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.5),
                                            offset: Offset(0.0, 3.0),
                                            blurRadius: 10.0,
                                            spreadRadius: 3.0,
                                          )
                                        ],
                                        color: Colors.white,
                                        borderRadius: new BorderRadius.all(
                                            Radius.circular(5.0)),
                                      ),
                                      child: Column(
                                        children: [
                                          TextFormField(
                                            controller: _a2,
                                            decoration: InputDecoration(
                                                labelText: "Amenaza"),
                                          ),
                                          Row(
                                            children: [
                                              Expanded(child: Text("PESO")),
                                              Expanded(
                                                child: TextFormField(
                                                  controller: _p17,
                                                  decoration: InputDecoration(
                                                      labelText: "[0.0 - 1]"),
                                                ),
                                              )
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              Expanded(
                                                  child: Text("CALIFICACIÓN")),
                                              Expanded(
                                                  child: DropdownButton<String>(
                                                      value: dropDownValue17,
                                                      items: <String>[
                                                        '1',
                                                        '2',
                                                        '3',
                                                        '4'
                                                      ].map<
                                                              DropdownMenuItem<
                                                                  String>>(
                                                          (String value) {
                                                        return DropdownMenuItem<
                                                            String>(
                                                          child: Text(value),
                                                          value: value,
                                                        );
                                                      }).toList(),
                                                      onChanged:
                                                          (String newValue) {
                                                        setState(() {
                                                          dropDownValue17 =
                                                              newValue;
                                                        });
                                                      }))
                                            ],
                                          ),
                                        ],
                                      )),
                                  new Container(
                                      height: 200.0,
                                      padding: EdgeInsets.all(15),
                                      margin: const EdgeInsets.only(
                                          top: 10.0,
                                          left: 10.0,
                                          right: 10.0,
                                          bottom: 10.0),
                                      decoration: BoxDecoration(
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.5),
                                            offset: Offset(0.0, 3.0),
                                            blurRadius: 10.0,
                                            spreadRadius: 3.0,
                                          )
                                        ],
                                        color: Colors.white,
                                        borderRadius: new BorderRadius.all(
                                            Radius.circular(5.0)),
                                      ),
                                      child: Column(
                                        children: [
                                          TextFormField(
                                            controller: _a3,
                                            decoration: InputDecoration(
                                                labelText: "Amenaza"),
                                          ),
                                          Row(
                                            children: [
                                              Expanded(child: Text("PESO")),
                                              Expanded(
                                                child: TextFormField(
                                                  controller: _p18,
                                                  decoration: InputDecoration(
                                                      labelText: "[0.0 - 1]"),
                                                ),
                                              )
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              Expanded(
                                                  child: Text("CALIFICACIÓN")),
                                              Expanded(
                                                  child: DropdownButton<String>(
                                                      value: dropDownValue18,
                                                      items: <String>[
                                                        '1',
                                                        '2',
                                                        '3',
                                                        '4'
                                                      ].map<
                                                              DropdownMenuItem<
                                                                  String>>(
                                                          (String value) {
                                                        return DropdownMenuItem<
                                                            String>(
                                                          child: Text(value),
                                                          value: value,
                                                        );
                                                      }).toList(),
                                                      onChanged:
                                                          (String newValue) {
                                                        setState(() {
                                                          dropDownValue18 =
                                                              newValue;
                                                        });
                                                      }))
                                            ],
                                          ),
                                        ],
                                      )),
                                  new Container(
                                      height: 200.0,
                                      padding: EdgeInsets.all(15),
                                      margin: const EdgeInsets.only(
                                          top: 10.0,
                                          left: 10.0,
                                          right: 10.0,
                                          bottom: 10.0),
                                      decoration: BoxDecoration(
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.5),
                                            offset: Offset(0.0, 3.0),
                                            blurRadius: 10.0,
                                            spreadRadius: 3.0,
                                          )
                                        ],
                                        color: Colors.white,
                                        borderRadius: new BorderRadius.all(
                                            Radius.circular(5.0)),
                                      ),
                                      child: Column(
                                        children: [
                                          TextFormField(
                                            controller: _a4,
                                            decoration: InputDecoration(
                                                labelText: "Amenaza"),
                                          ),
                                          Row(
                                            children: [
                                              Expanded(child: Text("PESO")),
                                              Expanded(
                                                child: TextFormField(
                                                  controller: _p19,
                                                  decoration: InputDecoration(
                                                      labelText: "[0.0 - 1]"),
                                                ),
                                              )
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              Expanded(
                                                  child: Text("CALIFICACIÓN")),
                                              Expanded(
                                                  child: DropdownButton<String>(
                                                      value: dropDownValue19,
                                                      items: <String>[
                                                        '1',
                                                        '2',
                                                        '3',
                                                        '4'
                                                      ].map<
                                                              DropdownMenuItem<
                                                                  String>>(
                                                          (String value) {
                                                        return DropdownMenuItem<
                                                            String>(
                                                          child: Text(value),
                                                          value: value,
                                                        );
                                                      }).toList(),
                                                      onChanged:
                                                          (String newValue) {
                                                        setState(() {
                                                          dropDownValue19 =
                                                              newValue;
                                                        });
                                                      }))
                                            ],
                                          ),
                                        ],
                                      )),
                                  new Container(
                                      height: 200.0,
                                      padding: EdgeInsets.all(15),
                                      margin: const EdgeInsets.only(
                                          top: 10.0,
                                          left: 10.0,
                                          right: 10.0,
                                          bottom: 10.0),
                                      decoration: BoxDecoration(
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.5),
                                            offset: Offset(0.0, 3.0),
                                            blurRadius: 10.0,
                                            spreadRadius: 3.0,
                                          )
                                        ],
                                        color: Colors.white,
                                        borderRadius: new BorderRadius.all(
                                            Radius.circular(5.0)),
                                      ),
                                      child: Column(
                                        children: [
                                          TextFormField(
                                            controller: _a5,
                                            decoration: InputDecoration(
                                                labelText: "Amenaza"),
                                          ),
                                          Row(
                                            children: [
                                              Expanded(child: Text("PESO")),
                                              Expanded(
                                                child: TextFormField(
                                                  controller: _p20,
                                                  decoration: InputDecoration(
                                                      labelText: "[0.0 - 1]"),
                                                ),
                                              )
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              Expanded(
                                                  child: Text("CALIFICACIÓN")),
                                              Expanded(
                                                  child: DropdownButton<String>(
                                                      value: dropDownValue20,
                                                      items: <String>[
                                                        '1',
                                                        '2',
                                                        '3',
                                                        '4'
                                                      ].map<
                                                              DropdownMenuItem<
                                                                  String>>(
                                                          (String value) {
                                                        return DropdownMenuItem<
                                                            String>(
                                                          child: Text(value),
                                                          value: value,
                                                        );
                                                      }).toList(),
                                                      onChanged:
                                                          (String newValue) {
                                                        setState(() {
                                                          dropDownValue20 =
                                                              newValue;
                                                        });
                                                      }))
                                            ],
                                          ),
                                        ],
                                      )),
                                ],
                              ),
                            ],
                          ),
                          isActive: _currentStep >= 0,
                          state: _currentStep >= 2
                              ? StepState.complete
                              : StepState.disabled,
                        ),
                      ]),
                ),
        ]));
  }
}
