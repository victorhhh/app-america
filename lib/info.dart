
//import 'package:empresa/expansionTile.dart';
import 'package:flutter/material.dart';




class AccountPage extends StatefulWidget {
  @override
  _AccountPageState createState() => new _AccountPageState();
}

class _AccountPageState extends State<AccountPage> {
    int currentStep = 0;
  bool complete = false;

  List<Step> steps = [

    Step(
      title: const Text('Identidad'),
      isActive: true,//currentStep == 0 ? true : false,
      //state: StepState.editing,
      content: Column(
        children: <Widget>[
          TextFormField(
            decoration: InputDecoration(labelText: 'Nombre de la empresa'),
          ),
          TextFormField(
            decoration: InputDecoration(labelText: 'Misión'),
          ),
          TextFormField(
            decoration: InputDecoration(labelText: 'Vsión'),
          ),
          TextFormField(
            decoration: InputDecoration(labelText: 'Valores'),
          ),
        ],
      ),
    ),
    Step(
      //isActive: false,
      //state: StepState.editing,
      title: const Text('Objetivos'),
      content: Column(
        children: <Widget>[
          TextFormField(
            decoration: InputDecoration(labelText: 'Objetivo a largo plazo'),
          ),
          ExpansionTile(
            title: Text("Objetivo a mediano plazo"),
            children: [
              TextFormField(
                decoration: InputDecoration(labelText: 'Primer objetivo a mediano plazo'),
              ),
              ExpansionTile(
                title: Text("Objetivos a corto plazo"),
                children: [
                  TextFormField(
                    decoration: InputDecoration(labelText: 'Primero objetivo a corto plazo'),
                  ),

                  TextFormField(
                    decoration: InputDecoration(labelText: 'Segundo objetivo a corto plazo'),
                  ),
                  TextFormField(
                    decoration: InputDecoration(labelText: 'Tercer objetivo a corto plazo'),
                  ),
                ],
              ),
              TextFormField(
                decoration: InputDecoration(labelText: 'Primer objetivo a mediano plazo'),
              ),
              ExpansionTile(
                title: Text("Objetivos a corto plazo"),
                children: [
                  TextFormField(
                    decoration: InputDecoration(labelText: 'Segundo objetivo a corto plazo'),
                  ),

                  TextFormField(
                    decoration: InputDecoration(labelText: 'Segundo objetivo a corto plazo'),
                  ),
                  TextFormField(
                    decoration: InputDecoration(labelText: 'Tercer objetivo a corto plazo'),
                  ),
                ],
              ),
              TextFormField(
                decoration: InputDecoration(labelText: 'Tercer objetivo a mediano plazo'),
              ),
              ExpansionTile(
                title: Text("Objetivos a corto plazo"),
                children: [
                  TextFormField(
                    decoration: InputDecoration(labelText: 'Primero objetivo a corto plazo'),
                  ),

                  TextFormField(
                    decoration: InputDecoration(labelText: 'Segundo objetivo a corto plazo'),
                  ),
                  TextFormField(
                    decoration: InputDecoration(labelText: 'Tercer objetivo a corto plazo'),
                  ),
                ],
              )
            ],
          ),
          TextFormField(
            decoration: InputDecoration(labelText: 'Objetivo a largo plazo'),
          ),
          ExpansionTile(
            title: Text("Objetivo a mediano plazo"),
            children: [
              TextFormField(
                decoration: InputDecoration(labelText: 'Primer objetivo a mediano plazo'),
              ),
              ExpansionTile(
                title: Text("Objetivos a corto plazo"),
                children: [
                  TextFormField(
                    decoration: InputDecoration(labelText: 'Primero objetivo a corto plazo'),
                  ),

                  TextFormField(
                    decoration: InputDecoration(labelText: 'Segundo objetivo a corto plazo'),
                  ),
                  TextFormField(
                    decoration: InputDecoration(labelText: 'Tercer objetivo a corto plazo'),
                  ),
                ],
              ),
              TextFormField(
                decoration: InputDecoration(labelText: 'Primer objetivo a mediano plazo'),
              ),
              ExpansionTile(
                title: Text("Objetivos a corto plazo"),
                children: [
                  TextFormField(
                    decoration: InputDecoration(labelText: 'Segundo objetivo a corto plazo'),
                  ),

                  TextFormField(
                    decoration: InputDecoration(labelText: 'Segundo objetivo a corto plazo'),
                  ),
                  TextFormField(
                    decoration: InputDecoration(labelText: 'Tercer objetivo a corto plazo'),
                  ),
                ],
              ),
              TextFormField(
                decoration: InputDecoration(labelText: 'Tercer objetivo a mediano plazo'),
              ),
              ExpansionTile(
                title: Text("Objetivos a corto plazo"),
                children: [
                  TextFormField(
                    decoration: InputDecoration(labelText: 'Primero objetivo a corto plazo'),
                  ),

                  TextFormField(
                    decoration: InputDecoration(labelText: 'Segundo objetivo a corto plazo'),
                  ),
                  TextFormField(
                    decoration: InputDecoration(labelText: 'Tercer objetivo a corto plazo'),
                  ),
                ],
              )
            ],
          ),
          TextFormField(
            decoration: InputDecoration(labelText: 'Objetivo a largo plazo'),
          ),
          ExpansionTile(
            title: Text("Objetivo a mediano plazo"),
            children: [
              TextFormField(
                decoration: InputDecoration(labelText: 'Primer objetivo a mediano plazo'),
              ),
              ExpansionTile(
                title: Text("Objetivos a corto plazo"),
                children: [
                  TextFormField(
                    decoration: InputDecoration(labelText: 'Primero objetivo a corto plazo'),
                  ),

                  TextFormField(
                    decoration: InputDecoration(labelText: 'Segundo objetivo a corto plazo'),
                  ),
                  TextFormField(
                    decoration: InputDecoration(labelText: 'Tercer objetivo a corto plazo'),
                  ),
                ],
              ),
              TextFormField(
                decoration: InputDecoration(labelText: 'Primer objetivo a mediano plazo'),
              ),
              ExpansionTile(
                title: Text("Objetivos a corto plazo"),
                children: [
                  TextFormField(
                    decoration: InputDecoration(labelText: 'Segundo objetivo a corto plazo'),
                  ),

                  TextFormField(
                    decoration: InputDecoration(labelText: 'Segundo objetivo a corto plazo'),
                  ),
                  TextFormField(
                    decoration: InputDecoration(labelText: 'Tercer objetivo a corto plazo'),
                  ),
                ],
              ),
              TextFormField(
                decoration: InputDecoration(labelText: 'Tercer objetivo a mediano plazo'),
              ),
              ExpansionTile(
                title: Text("Objetivos a corto plazo"),
                children: [
                  TextFormField(
                    decoration: InputDecoration(labelText: 'Primero objetivo a corto plazo'),
                  ),

                  TextFormField(
                    decoration: InputDecoration(labelText: 'Segundo objetivo a corto plazo'),
                  ),
                  TextFormField(
                    decoration: InputDecoration(labelText: 'Tercer objetivo a corto plazo'),
                  ),
                ],
              )
            ],
          ),

        ],
      ),
    ),
    Step(
      //state: StepState.editing,
      isActive: false,
      title: const Text('FODA'),
      content: Column(
        children: <Widget>[
          ExpansionTile(
            title: Text("Fortalezas"),
            children: [
              TextFormField(
                decoration: InputDecoration(labelText: "Fortalezas"),
              )
            ],
          ),
          ExpansionTile(
            title: Text("Oportunidades"),
            children: [
              TextFormField(
                decoration: InputDecoration(labelText: "Oportunidades"),
              )
            ],
          ),
          ExpansionTile(
            title: Text("Debilidades"),
            children: [
              TextFormField(
                decoration: InputDecoration(labelText: "Debilidades"),
              )
            ],
          ),
          ExpansionTile(
            title: Text("Amenzas"),
            children: [
              TextFormField(
                decoration: InputDecoration(labelText: "Amenzas"),
              )
            ],
          )
        ],
      ),
    ),
  ];

  StepperType stepperType = StepperType.horizontal;

 
  next() {
    currentStep + 1 != steps.length
        ? goTo(currentStep + 1)
        : setState(() => complete = true);
  }

  cancel() {
    if (currentStep > 0) {
      goTo(currentStep - 1);
    }
  }

  goTo(int step) {
    setState(() => currentStep = step);
  }
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
          title: Text('Beauty Skin'),
        ),
        body: Column(children: <Widget>[
          complete
              ? Expanded(
            child: Center(
              child: AlertDialog(
                title: Text("Finalizado"),
                content: Text("Chevere"),
                actions: [
                  FlatButton(
                    child: Text("Cerrar"),
                    onPressed: () => {
                      setState(() => complete = false)
                    },
                  )
                ],
              ),
            ),
          )
              : Expanded(
            child: Stepper(
              steps: steps,
              type: stepperType,
              currentStep: currentStep,
              onStepContinue: next,
              onStepCancel: cancel,
              onStepTapped: (step) => goTo(step),
            ),
          ),
        ]));
  }
}